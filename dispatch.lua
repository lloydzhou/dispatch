--[[
	@author Lloyd Zhou
	@email lloydzhou@qq.com
	@license MIT
--]]

local error_callbacks = {}
local error = function (code, callback_or_message)
	if callback_or_message and type(callback_or_message) == "function" then
		error_callbacks[code] = callback_or_message
	else
		print ("call from local.")
		local cb = error_callbacks[code] or error
		cb(callback_or_message, code)
	end
end

--error(404, function(message, code) print (message) print(code) end)
--error(404, "test message2")

local _config = {}
local config = function (key, value)
	if key == nil or type(key) == "table" then
		_config = key or {}
	else
		if value == nil then
			return _config[key]
		end
		_config[key] = value
	end
end
--[[
config('k1', 'v1')
config('k2', 'v2')
print(config('k1'))
print(config('k2'))
config()
print(config('k2'))
--]]

local routes = {}
local on = function (path, callback)
	if type(callback) then 
		routes[path] = callback
	else
	end
end